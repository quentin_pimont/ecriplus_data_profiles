from configparser import ConfigParser

def format_json(json:str):
    format_json = json.replace('[{', '[\n\t{')
    format_json = format_json.replace('}]', '}\n]')
    format_json = format_json.replace('}, ', '},\n\t')
    return format_json

def get_env(envFile = '.env', section='airtable'):
    parser = ConfigParser()
    parser.read(envFile)
    env = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            env[param[0]] = param[1]
    return env