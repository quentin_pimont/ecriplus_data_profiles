from airtable import Airtable
from data_utils import format_json, get_env
import json
env = get_env()
skillsTable = Airtable(env['base_key'], table_name= 'Acquis', api_key=env['api_key'])
challengeTable = Airtable(env['base_key'], table_name= 'Epreuves', api_key=env['api_key'])
domainsTable = Airtable(env['base_key'], table_name= 'Domaines', api_key=env['api_key'])
skillsBrut = skillsTable.get_all()
allDomains = domainsTable.get_all()
competenceChallengeLen = {}
certifiableProfilsData = []
certifChallengeData = []
certifSuccessData = []
certifFailureData = []
certifSuccessCompetenceMarkData = []
certifFailureCompetenceMarkData = []
for skill in skillsBrut:
    if 'Compétence (via Tube) (id persistant)' not in skill['fields']:
        continue
    # partie pour rendre un profils certifiable
    skillId = skill['fields']['id persistant']
    skillName = skill['fields']['Nom']
    earnedPix = skill['fields']['PixValue']
    competenceId = skill['fields']['Compétence (via Tube) (id persistant)'][0]
    challenge = challengeTable.search('Record Id', skill['fields']['Epreuves'][0])
    challengeId = challenge[0]['fields']['id persistant']
    rowProfilsData = { 
        'source': 'direct',
        'skillId': skillId,
        'earnedPix': earnedPix,
        'status': 'validated',
        'competenceId': competenceId,
        'result': 'ok',
        'challengeId': challengeId, 
        'elapsedTime': 0,
        'timeout': None,
        'resultDetails': 'null' 
    }
    certifiableProfilsData.append(rowProfilsData)
    
    # partie pour les réponse dans la sessions de certification
    if competenceId not in competenceChallengeLen or competenceChallengeLen[competenceId] < 4:
        if competenceId not in competenceChallengeLen:
            competenceChallengeLen[competenceId] = 1
        else:
            competenceChallengeLen[competenceId] += 1
        rowChallengeData = {
            'challengeId': challengeId,
            'competenceId': competenceId,
            'associatedSkillName': skillName
        }
        rowCertifSuccessData = {
            'result': 'ok',
            'challengeId': challengeId,
            'elapsedTime': '0',
            'resultDetails': 'null'
        }
        rowCertifFailureData = {
            'result': 'ko',
            'challengeId': challengeId,
            'elapsedTime': '0',
            'resultDetails': 'null'
        }
        certifChallengeData.append(rowChallengeData)
        certifSuccessData.append(rowCertifSuccessData)
        certifFailureData.append(rowCertifFailureData)

for domain in allDomains:
    domainCode = domain['fields']['Code']
    allCompetencesIds = domain['fields']['Competences (identifiants) (id persistant)']
    for compId in allCompetencesIds:
        rowSuccessureMark = {
            'level': 5,
            'score': 40,
            'area_code': domainCode,
            'competence_code': compId,
            'competenceId': compId
        }
        rowFailureMark = {
            'level': -1,
            'score': 0,
            'area_code': domainCode,
            'competence_code': compId,
            'competenceId': compId
        }
        certifSuccessCompetenceMarkData.append(rowSuccessureMark)
        certifFailureCompetenceMarkData.append(rowFailureMark)

with open('data.js', 'w') as data_file:
    data_file.writelines('const CERTIFIABLE_USER_DATA = ' + format_json(json.dumps(certifiableProfilsData)) + ';\n\n')
    data_file.writelines('const CERTIFICATION_CHALLENGES_DATA = ' + format_json(json.dumps(certifChallengeData)) + ';\n\n')
    data_file.writelines('const CERTIFICATION_SUCCESS_ANSWERS_DATA = ' + format_json(json.dumps(certifSuccessData)) + ';\n\n')
    data_file.writelines('const CERTIFICATION_FAILURE_ANSWERS_DATA = ' + format_json(json.dumps(certifFailureData)) + ';\n\n')
    data_file.writelines('const CERTIFICATION_SUCCESS_COMPETENCE_MARKS_DATA = ' + format_json(json.dumps(certifSuccessCompetenceMarkData)) + ';\n\n')
    data_file.writelines('const CERTIFICATION_FAILURE_COMPETENCE_MARKS_DATA = ' + format_json(json.dumps(certifFailureCompetenceMarkData)) + ';\n\n')
    data_file.writelines('module.exports = {\n\tCERTIFIABLE_USER_DATA,\n\tCERTIFICATION_CHALLENGES_DATA,\n\tCERTIFICATION_SUCCESS_ANSWERS_DATA,\n\t'
    + 'CERTIFICATION_FAILURE_ANSWERS_DATA,\n\tCERTIFICATION_SUCCESS_COMPETENCE_MARKS_DATA,\n\tCERTIFICATION_FAILURE_COMPETENCE_MARKS_DATA\n};')
    data_file.close()